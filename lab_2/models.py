from django.db import models

# Create your models here.
class Note(models.Model):
    # TODO Implement missing attributes in Friend model
    to = models.CharField(max_length=600)
    from_who = models.CharField(max_length=600)
    title = models.CharField(max_length=600)
    message = models.CharField(max_length=600)