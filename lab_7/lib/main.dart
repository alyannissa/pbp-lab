// import 'package:flutter/material.dart';

// void main() {
//   runApp(const MyApp());
// }

// class MyApp extends StatelessWidget {
//   const MyApp({Key? key}) : super(key: key);

//   // This widget is the root of your application.
//   @override
//   Widget build(BuildContext context) {
//     return MaterialApp(
//       title: 'Flutter Demo',
//       theme: ThemeData(
//         // This is the theme of your application.
//         //
//         // Try running your application with "flutter run". You'll see the
//         // application has a blue toolbar. Then, without quitting the app, try
//         // changing the primarySwatch below to Colors.green and then invoke
//         // "hot reload" (press "r" in the console where you ran "flutter run",
//         // or simply save your changes to "hot reload" in a Flutter IDE).
//         // Notice that the counter didn't reset back to zero; the application
//         // is not restarted.
//         primarySwatch: Colors.blue,
//       ),
//       home: const MyHomePage(title: 'Flutter Demo Home Page'),
//     );
//   }
// }

// class MyHomePage extends StatefulWidget {
//   const MyHomePage({Key? key, required this.title}) : super(key: key);

//   // This widget is the home page of your application. It is stateful, meaning
//   // that it has a State object (defined below) that contains fields that affect
//   // how it looks.

//   // This class is the configuration for the state. It holds the values (in this
//   // case the title) provided by the parent (in this case the App widget) and
//   // used by the build method of the State. Fields in a Widget subclass are
//   // always marked "final".

//   final String title;

//   @override
//   State<MyHomePage> createState() => _MyHomePageState();
// }

// class _MyHomePageState extends State<MyHomePage> {
//   int _counter = 0;

//   void _incrementCounter() {
//     setState(() {
//       // This call to setState tells the Flutter framework that something has
//       // changed in this State, which causes it to rerun the build method below
//       // so that the display can reflect the updated values. If we changed
//       // _counter without calling setState(), then the build method would not be
//       // called again, and so nothing would appear to happen.
//       _counter++;
//     });
//   }

//   @override
//   Widget build(BuildContext context) {
//     // This method is rerun every time setState is called, for instance as done
//     // by the _incrementCounter method above.
//     //
//     // The Flutter framework has been optimized to make rerunning build methods
//     // fast, so that you can just rebuild anything that needs updating rather
//     // than having to individually change instances of widgets.
//     return Scaffold(
//       appBar: AppBar(
//         // Here we take the value from the MyHomePage object that was created by
//         // the App.build method, and use it to set our appbar title.
//         title: Text(widget.title),
//       ),
//       body: Center(
//         // Center is a layout widget. It takes a single child and positions it
//         // in the middle of the parent.
//         child: Column(
//           // Column is also a layout widget. It takes a list of children and
//           // arranges them vertically. By default, it sizes itself to fit its
//           // children horizontally, and tries to be as tall as its parent.
//           //
//           // Invoke "debug painting" (press "p" in the console, choose the
//           // "Toggle Debug Paint" action from the Flutter Inspector in Android
//           // Studio, or the "Toggle Debug Paint" command in Visual Studio Code)
//           // to see the wireframe for each widget.
//           //
//           // Column has various properties to control how it sizes itself and
//           // how it positions its children. Here we use mainAxisAlignment to
//           // center the children vertically; the main axis here is the vertical
//           // axis because Columns are vertical (the cross axis would be
//           // horizontal).
//           mainAxisAlignment: MainAxisAlignment.center,
//           children: <Widget>[
//             const Text(
//               'You have pushed the button this many times:',
//             ),
//             Text(
//               '$_counter',
//               style: Theme.of(context).textTheme.headline4,
//             ),
//           ],
//         ),
//       ),
//       floatingActionButton: FloatingActionButton(
//         onPressed: _incrementCounter,
//         tooltip: 'Increment',
//         child: const Icon(Icons.add),
//       ), // This trailing comma makes auto-formatting nicer for build methods.
//     );
//   }
// }

// import 'package:flutter/material.dart';

// void main() {
//   runApp(const MyApp());
// }

// class MyApp extends StatelessWidget {
//   const MyApp({Key? key}) : super(key: key);

//   // This widget is the root of your application.
//   @override
//   Widget build(BuildContext context) {
//     return MaterialApp(
//       title: 'Flutter Demo',
//       theme: ThemeData(
//         primarySwatch: Colors.blue,
//       ),
//       home: const MyHomePage(),
//     );
//   }
// }

// class MyHomePage extends StatefulWidget {
//   const MyHomePage({Key? key}) : super(key: key);

//   @override
//   _MyHomePageState createState() => _MyHomePageState();
// }

// class _MyHomePageState extends State<MyHomePage> {
//   int _activeStepIndex = 0;

//   TextEditingController name = TextEditingController();
//   TextEditingController email = TextEditingController();
//   TextEditingController pass = TextEditingController();
//   TextEditingController address = TextEditingController();
//   TextEditingController pincode = TextEditingController();

//   List<Step> stepList() => [
//         Step(
//           state: _activeStepIndex <= 0 ? StepState.editing : StepState.complete,
//           isActive: _activeStepIndex >= 0,
//           title: const Text('Account'),
//           content: Container(
//             child: Column(
//               children: [
//                 TextField(
//                   controller: name,
//                   decoration: const InputDecoration(
//                     border: OutlineInputBorder(),
//                     labelText: 'Full Name',
//                   ),
//                 ),
//                 const SizedBox(
//                   height: 8,
//                 ),
//                 TextField(
//                   controller: email,
//                   decoration: const InputDecoration(
//                     border: OutlineInputBorder(),
//                     labelText: 'Email',
//                   ),
//                 ),
//                 const SizedBox(
//                   height: 8,
//                 ),
//                 TextField(
//                   controller: pass,
//                   obscureText: true,
//                   decoration: const InputDecoration(
//                     border: OutlineInputBorder(),
//                     labelText: 'Password',
//                   ),
//                 ),
//               ],
//             ),
//           ),
//         ),
//         Step(
//             state:
//                 _activeStepIndex <= 1 ? StepState.editing : StepState.complete,
//             isActive: _activeStepIndex >= 1,
//             title: const Text('Address'),
//             content: Container(
//               child: Column(
//                 children: [
//                   const SizedBox(
//                     height: 8,
//                   ),
//                   TextField(
//                     controller: address,
//                     decoration: const InputDecoration(
//                       border: OutlineInputBorder(),
//                       labelText: 'Full House Address',
//                     ),
//                   ),
//                   const SizedBox(
//                     height: 8,
//                   ),
//                   TextField(
//                     controller: pincode,
//                     decoration: const InputDecoration(
//                       border: OutlineInputBorder(),
//                       labelText: 'Pin Code',
//                     ),
//                   ),
//                 ],
//               ),
//             )),
//         Step(
//             state: StepState.complete,
//             isActive: _activeStepIndex >= 2,
//             title: const Text('Confirm'),
//             content: Container(
//                 child: Column(
//               crossAxisAlignment: CrossAxisAlignment.start,
//               mainAxisAlignment: MainAxisAlignment.start,
//               children: [
//                 Text('Name: ${name.text}'),
//                 Text('Email: ${email.text}'),
//                 const Text('Password: *****'),
//                 Text('Address : ${address.text}'),
//                 Text('PinCode : ${pincode.text}'),
//               ],
//             )))
//       ];

//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//       appBar: AppBar(
//         title: const Text('Submit Form'),
//       ),
//       body: Stepper(
//         type: StepperType.vertical,
//         currentStep: _activeStepIndex,
//         steps: stepList(),
//         onStepContinue: () {
//           if (_activeStepIndex < (stepList().length - 1)) {
//             setState(() {
//               _activeStepIndex += 1;
//             });
//           } else {
//             print('Submited');
//           }
//         },
//         onStepCancel: () {
//           if (_activeStepIndex == 0) {
//             return;
//           }

//           setState(() {
//             _activeStepIndex -= 1;
//           });
//         },
//         onStepTapped: (int index) {
//           setState(() {
//             _activeStepIndex = index;
//           });
//         },
//         controlsBuilder: (context, {onStepContinue, onStepCancel}) {
//           final isLastStep = _activeStepIndex == stepList().length - 1;
//           return Container(
//             child: Row(
//               children: [
//                 Expanded(
//                   child: ElevatedButton(
//                     onPressed: onStepContinue,
//                     child: (isLastStep)
//                         ? const Text('Submit')
//                         : const Text('Next'),
//                   ),
//                 ),
//                 const SizedBox(
//                   width: 10,
//                 ),
//                 if (_activeStepIndex > 0)
//                   Expanded(
//                     child: ElevatedButton(
//                       onPressed: onStepCancel,
//                       child: const Text('Back'),
//                     ),
//                   )
//               ],
//             ),
//           );
//         },
//       ),
//     );
//   }
// }
//-------------------------------------------------------------------------
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

Future main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await SystemChrome.setPreferredOrientations([
    DeviceOrientation.portraitUp,
    DeviceOrientation.portraitDown,
  ]);

  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  static final String title = 'Feedback Form';

  @override
  Widget build(BuildContext context) => MaterialApp(
        debugShowCheckedModeBanner: false,
        title: title,
        theme: ThemeData(primarySwatch: Colors.blue),
        home: MainPage(),
      );
}

class MainPage extends StatefulWidget {
  @override
  _MainPageState createState() => _MainPageState();
}

class _MainPageState extends State<MainPage> {
  int currentStep = 0;
  bool isCompleted = false;

  final firstName = TextEditingController();
  final lastName = TextEditingController();
  final address = TextEditingController();
  final postcode = TextEditingController();

  @override
  Widget build(BuildContext context) => Scaffold(
        appBar: AppBar(
          title: Text(MyApp.title),
          centerTitle: true,
        ),
        body: isCompleted
            ? buildCompleted()
            : Theme(
                data: Theme.of(context).copyWith(
                  colorScheme: ColorScheme.light(primary: Colors.red),
                ),
                child: Stepper(
                  type: StepperType.horizontal,
                  steps: getSteps(),
                  currentStep: currentStep,
                  onStepContinue: () {
                    final isLastStep = currentStep == getSteps().length - 1;

                    if (isLastStep) {
                      setState(() => isCompleted = true);
                      print('Completed');

                      /// send data to server
                    } else {
                      setState(() => currentStep += 1);
                    }
                  },
                  onStepTapped: (step) => setState(() => currentStep = step),
                  onStepCancel: currentStep == 0
                      ? null
                      : () => setState(() => currentStep -= 1),
                  controlsBuilder: (context, {onStepContinue, onStepCancel}) {
                    final isLastStep = currentStep == getSteps().length - 1;

                    return Container(
                      margin: EdgeInsets.only(top: 50),
                      child: Row(
                        children: [
                          Expanded(
                            child: ElevatedButton(
                              child: Text(isLastStep ? 'CONFIRM' : 'NEXT'),
                              onPressed: onStepContinue,
                            ),
                          ),
                          const SizedBox(width: 12),
                          if (currentStep != 0)
                            Expanded(
                              child: ElevatedButton(
                                child: Text('BACK'),
                                onPressed: onStepCancel,
                              ),
                            ),
                        ],
                      ),
                    );
                  },
                ),
              ),
      );

  List<Step> getSteps() => [
        Step(
          state: currentStep > 0 ? StepState.complete : StepState.indexed,
          isActive: currentStep >= 0,
          title: Text('Account'),
          content: Column(
            children: <Widget>[
              TextFormField(
                controller: firstName,
                decoration: InputDecoration(labelText: 'First Name'),
              ),
              TextFormField(
                controller: lastName,
                decoration: InputDecoration(labelText: 'Last Name'),
              ),
            ],
          ),
        ),
        Step(
          state: currentStep > 1 ? StepState.complete : StepState.indexed,
          isActive: currentStep >= 1,
          title: Text('Feedbacks'),
          content: Column(
            children: <Widget>[
              TextFormField(
                controller: address,
                decoration: InputDecoration(labelText: 'Write Your Message Here'),
              ),
              TextFormField(
                controller: postcode,
                decoration: InputDecoration(labelText: 'Write Your Feedbacks Here'),
              ),
            ],
          ),
        ),
        Step(
          isActive: currentStep >= 2,
          title: Text('Complete'),
          content: Column(
            children: <Widget>[
              buildText('First Name', firstName.text),
              const SizedBox(height: 12),
              buildText('Last Name', lastName.text),
              const SizedBox(height: 12),
              buildText('Message', address.text),
              const SizedBox(height: 12),
              buildText('Message', postcode.text),
            ],
          ),
        ),
      ];

  Widget buildCompleted() => Container(
        alignment: Alignment.center,
        margin: EdgeInsets.symmetric(horizontal: 52),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Icon(Icons.cloud_done, color: Colors.blue, size: 120),
            const SizedBox(height: 8),
            Text(
              'Success',
              style: TextStyle(
                fontSize: 40,
                fontWeight: FontWeight.bold,
                color: Colors.blue,
              ),
            ),
            const SizedBox(height: 64),
            ElevatedButton(
              style: ElevatedButton.styleFrom(
                primary: Colors.black,
                textStyle: TextStyle(fontSize: 24),
                minimumSize: Size.fromHeight(50),
              ),
              child: Text('Reset'),
              onPressed: () => setState(() {
                isCompleted = false;
                currentStep = 0;

                firstName.clear();
                lastName.clear();
                address.clear();
                postcode.clear();
              }),
            ),
          ],
        ),
      );

  Widget buildText(String title, String value) => Row(
        children: [
          Container(
            width: 140,
            child: Text(
              title,
              style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
            ),
          ),
          Text(
            value,
            style: TextStyle(fontSize: 20),
          ),
        ],
      );
}


// import 'package:flutter/material.dart';
// import 'package:flutter/services.dart';

// Future main() async {
//   WidgetsFlutterBinding.ensureInitialized();
//   await SystemChrome.setPreferredOrientations([
//     DeviceOrientation.portraitUp,
//     DeviceOrientation.portraitDown,
//   ]);

//   runApp(MyApp());
// }

// class MyApp extends StatelessWidget {
//   static final String title = 'Feedback Form';

//   @override
//   Widget build(BuildContext context) => MaterialApp(
//         debugShowCheckedModeBanner: false,
//         title: title,
//         theme: ThemeData(primarySwatch: Colors.blue),
//         home: MainPage(),
//       );
// }

// class MainPage extends StatefulWidget {
//   @override
//   _MainPageState createState() => _MainPageState();
// }

// class _MainPageState extends State<MainPage> {
//   int currentStep = 0;
//   bool isCompleted = false;

//   final firstName = TextEditingController();
//   final lastName = TextEditingController();
//   final address = TextEditingController();
//   final postcode = TextEditingController();

//   var result =[];

//   getItems() {
//     List.forEach(TextFormField) {
//       result.add(TextFormField);
//     }
//   }

//   @override
//   Widget build(BuildContext context) => Scaffold(
//         appBar: AppBar(
//           title: Text(MyApp.title),
//           centerTitle: true,
//         ),
//         body: isCompleted
//             ? buildCompleted()
//             : Theme(
//                 data: Theme.of(context).copyWith(
//                   colorScheme: ColorScheme.light(primary: Colors.red),
//                 ),
//                 child: Stepper(
//                   type: StepperType.horizontal,
//                   steps: getSteps(),
//                   currentStep: currentStep,
//                   onStepContinue: () {
//                     final isLastStep = currentStep == getSteps().length - 1;

//                     if (isLastStep) {
//                       setState(() => isCompleted = true);
//                       print('Completed');

//                       /// send data to server
//                     } else {
//                       setState(() => currentStep += 1);
//                     }
//                   },
//                   onStepTapped: (step) => setState(() => currentStep = step),
//                   onStepCancel: currentStep == 0
//                       ? null
//                       : () => setState(() => currentStep -= 1),
//                   controlsBuilder: (context, {onStepContinue, onStepCancel}) {
//                     final isLastStep = currentStep == getSteps().length - 1;

//                     return Container(
//                       margin: EdgeInsets.only(top: 50),
//                       child: Row(
//                         children: [
//                           Expanded(
//                             child: ElevatedButton(
//                               child: Text(isLastStep ? 'CONFIRM' : 'NEXT'),
//                               onPressed: onStepContinue,
//                               print(result),
//                             ),
//                           ),
//                           const SizedBox(width: 12),
//                           if (currentStep != 0)
//                             Expanded(
//                               child: ElevatedButton(
//                                 child: Text('BACK'),
//                                 onPressed: onStepCancel,
//                               ),
//                             ),
//                         ],
//                       ),
//                     );
//                   },
//                 ),
//               ),
//       );

//   List<Step> result getSteps() => [
//         Step(
//           state: currentStep > 0 ? StepState.complete : StepState.indexed,
//           isActive: currentStep >= 0,
//           title: Text('Account'),
//           content: Column(
//             children: <Widget>[
//               TextFormField(
//                 controller: firstName,
//                 decoration: InputDecoration(labelText: 'First Name'),
//               ),
//               TextFormField(
//                 controller: lastName,
//                 decoration: InputDecoration(labelText: 'Last Name'),
//               ),
//             ],
//           ),
//         ),
//         Step(
//           state: currentStep > 1 ? StepState.complete : StepState.indexed,
//           isActive: currentStep >= 1,
//           title: Text('Feedbacks'),
//           content: Column(
//             children: <Widget>[
//               TextFormField(
//                 controller: address,
//                 decoration: InputDecoration(labelText: 'Write Your Message Here'),
//               ),
//               TextFormField(
//                 controller: postcode,
//                 decoration: InputDecoration(labelText: 'Write Your Feedbacks Here'),
//               ),
//             ],
//           ),
//         ),
//         Step(
//           isActive: currentStep >= 2,
//           title: Text('Complete'),
//           content: Column(
//             children: <Widget>[
//               buildText('First Name', firstName.text),
//               const SizedBox(height: 12),
//               buildText('Last Name', lastName.text),
//               const SizedBox(height: 12),
//               buildText('Message', address.text),
//               const SizedBox(height: 12),
//               buildText('Message', postcode.text),
//             ],
//           ),
//         ),
//       ];

//   Widget buildCompleted() => Container(
//         alignment: Alignment.center,
//         margin: EdgeInsets.symmetric(horizontal: 52),
//         child: Column(
//           mainAxisAlignment: MainAxisAlignment.center,
//           children: [
//             Icon(Icons.cloud_done, color: Colors.blue, size: 120),
//             const SizedBox(height: 8),
//             Text(
//               'Success',
//               style: TextStyle(
//                 fontSize: 40,
//                 fontWeight: FontWeight.bold,
//                 color: Colors.blue,
//               ),
//             ),
//             const SizedBox(height: 64),
//             ElevatedButton(
//               style: ElevatedButton.styleFrom(
//                 primary: Colors.black,
//                 textStyle: TextStyle(fontSize: 24),
//                 minimumSize: Size.fromHeight(50),
//               ),
//               child: Text('Reset'),
//               onPressed: () => setState(() {
//                 isCompleted = false;
//                 currentStep = 0;

//                 firstName.clear();
//                 lastName.clear();
//                 address.clear();
//                 postcode.clear();
//               }),
//             ),
//           ],
//         ),
//       );

//   Widget buildText(String title, String value) => Row(
//         children: [
//           Container(
//             width: 140,
//             child: Text(
//               title,
//               style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
//             ),
//           ),
//           Text(
//             value,
//             style: TextStyle(fontSize: 20),
//           ),
//         ],
//       );
// }