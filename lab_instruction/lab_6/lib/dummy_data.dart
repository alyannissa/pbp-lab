import 'package:flutter/material.dart';

import './models/category.dart';
import './models/meal.dart';

const DUMMY_CATEGORIES = const [
  Category(
    id: 'c1',
    title: 'ADIDAS',
    color: Colors.amber,
  ),
  Category(
    id: 'c2',
    title: 'ALEXANDER WANG',
    color: Colors.amber,
  ),
  Category(
    id: 'c3',
    title: 'BALENCIAGA',
    color: Colors.amber,
  ),
  Category(
    id: 'c4',
    title: 'BOTTEGA VENETA',
    color: Colors.amber,
  ),
  Category(
    id: 'c5',
    title: 'CHAMPION',
    color: Colors.amber,
  ),
  Category(
    id: 'c6',
    title: 'COMME DES GARÇONS',
    color: Colors.amber,
  ),
  Category(
    id: 'c7',
    title: 'DOLCE & GABBANA',
    color: Colors.amber,
  ),
  Category(
    id: 'c8',
    title: 'GIVENCHY',
    color: Colors.amber,
  ),
  Category(
    id: 'c9',
    title: 'JACQUEMUS',
    color: Colors.amber,
  ),
  Category(
    id: 'c10',
    title: 'KENZO',
    color: Colors.amber,
  ),
];

const DUMMY_MEALS = const [
  Meal(
    id: 'm1',
    categories: [
      'c1',
      'c2',
    ],
    title: 'ADIDAS',
    affordability: Affordability.Affordable,
    complexity: Complexity.Simple,
    imageUrl:
        'https://asset.kompas.com/crops/yNDm64AkDfOg5wDv3pBRZLnw89E=/182x0:857x450/750x500/data/photo/2020/01/16/5e2056c7ba28a.jpg',
    duration: 20,
    ingredients: [
      'Plaza Indonesia',
      'Gandaria City',
      'Pondok Indah Mall',
      'Plaza Senayan',
      'Lippo Mall Kemang',
      'Senayan City'
    ],
    steps: [
      'Essentials Fleece 3-Stripes Pants',
      'LOUNGEWEAR Essentials Logo Fleece Hoodie',
      'Ultraboost 21 Shoes',
    ],
    isGlutenFree: false,
    isVegan: true,
    isVegetarian: true,
    isLactoseFree: true,
  ),
  Meal(
    id: 'm2',
    categories: [
      'c2',
    ],
    title: 'ALEXANDER WANG',
    affordability: Affordability.Affordable,
    complexity: Complexity.Simple,
    imageUrl:
        'https://stylecaster.com/wp-content/uploads/2014/11/how-to-shop-alexander-wang-hm.jpg',
    duration: 10,
    ingredients: [
      'Plaza Indonesia',
      'Gandaria City',
      'Pondok Indah Mall',
      'Plaza Senayan',
      'Lippo Mall Kemang',
    ],
    steps: [
      'Heiress Medium Pouch',
      'Nova Studded Pcv Slingback Sandals',
      'Crystal Logo Jessie Mules'
    ],
    isGlutenFree: false,
    isVegan: false,
    isVegetarian: false,
    isLactoseFree: false,
  ),
  Meal(
    id: 'm3',
    categories: [
      'c2',
      'c3',
    ],
    title: 'BALENCIAGA',
    affordability: Affordability.Pricey,
    complexity: Complexity.Simple,
    imageUrl:
        'https://image-cdn.hypb.st/https%3A%2F%2Fhypebeast.com%2Fwp-content%2Fblogs.dir%2F6%2Ffiles%2F2018%2F05%2Ffarfetch-designer-bag-sale-jw-anderson-balenciaga-off-white-01.jpg',
    duration: 45,
    ingredients: [
      'Plaza Indonesia',
      'Gandaria City',
      'Pondok Indah Mall',
      'Plaza Senayan',
      'Lippo Mall Kemang',
      'Senayan City'
    ],
    steps: [
      'Hourglass XS croc-effect leather shoulder bag',
      'Hourglass nano croc-effect leather tote',
      'Neo Classic City nano croc-effect leather tote',
    ],
    isGlutenFree: false,
    isVegan: false,
    isVegetarian: false,
    isLactoseFree: true,
  ),
  Meal(
    id: 'm4',
    categories: [
      'c4',
    ],
    title: 'BOTTEGA VENETA',
    affordability: Affordability.Luxurious,
    complexity: Complexity.Challenging,
    imageUrl:
        'https://www.quivedo.com/media/catalog/category/BV_FW20_SUN_M_MAN.jpg',
    duration: 60,
    ingredients: [
      'Plaza Indonesia',
      'Gandaria City',
      'Pondok Indah Mall',
      'Plaza Senayan',
      'Lippo Mall Kemang',
      'Senayan City'
      'Lippo Mall Puri',
      'Lotte Mall',
      'Kuningan City'
    ],
    steps: [
      'Essentials Fleece 3-Stripes Pants',
      'LOUNGEWEAR Essentials Logo Fleece Hoodie',
      'Ultraboost 21 Shoes',
    ],
    isGlutenFree: false,
    isVegan: false,
    isVegetarian: false,
    isLactoseFree: false,
  ),
  Meal(
    id: 'm5',
    categories: [
      'c2'
          'c5',
      'c10',
    ],
    title: 'CHAMPION',
    affordability: Affordability.Luxurious,
    complexity: Complexity.Simple,
    imageUrl:
        'https://hips.hearstapps.com/hmg-prod.s3.amazonaws.com/images/champion-brand-breakdown-lead-1620944458.jpg?crop=1xw:1xh;center,top&resize=640:*',
    duration: 15,
    ingredients: [
      'Plaza Indonesia',
      'Gandaria City',
      'Pondok Indah Mall',
      'Plaza Senayan',
      'Lippo Mall Kemang',
      'Senayan City'
      'Lippo Mall Puri',
      'Lotte Mall',
      'Kuningan City'
    ],
    steps: [
      'Essentials Fleece 3-Stripes Pants',
      'LOUNGEWEAR Essentials Logo Fleece Hoodie',
      'Ultraboost 21 Shoes',
    ],
    isGlutenFree: true,
    isVegan: false,
    isVegetarian: true,
    isLactoseFree: true,
  ),
  Meal(
    id: 'm6',
    categories: [
      'c6',
      'c10',
    ],
    title: 'COMME DES GARÇONS',
    affordability: Affordability.Affordable,
    complexity: Complexity.Hard,
    imageUrl:
        'https://assets.teenvogue.com/photos/5ee10bd55a4e28aad739c54a/master/pass/GettyImages-1248006432.jpg',
    duration: 240,
    ingredients: [
      'Plaza Indonesia',
      'Gandaria City',
      'Pondok Indah Mall',
      'Plaza Senayan',
      'Lippo Mall Kemang',
      'Senayan City'
    ],
    steps: [
      'Essentials Fleece 3-Stripes Pants',
      'LOUNGEWEAR Essentials Logo Fleece Hoodie',
      'Ultraboost 21 Shoes',
    ],
    isGlutenFree: true,
    isVegan: false,
    isVegetarian: true,
    isLactoseFree: false,
  ),
  Meal(
    id: 'm7',
    categories: [
      'c7',
    ],
    title: 'DOLCE & GABBANA',
    affordability: Affordability.Affordable,
    complexity: Complexity.Simple,
    imageUrl:
        'https://mvcmagazine.com/wp-content/uploads/2020/09/mvccopdolcegabbana.jpg',
    duration: 20,
    ingredients: [
      'Plaza Indonesia',
      'Gandaria City',
      'Pondok Indah Mall',
      'Plaza Senayan',
      'Lippo Mall Kemang',
      'Senayan City'
      'Lippo Mall Puri',
      'Lotte Mall',
      'Kuningan City'
    ],
    steps: [
      'Essentials Fleece 3-Stripes Pants',
      'LOUNGEWEAR Essentials Logo Fleece Hoodie',
      'Ultraboost 21 Shoes',
    ],
    isGlutenFree: true,
    isVegan: false,
    isVegetarian: true,
    isLactoseFree: false,
  ),
  Meal(
    id: 'm8',
    categories: [
      'c8',
    ],
    title: 'GIVENCHY',
    affordability: Affordability.Pricey,
    complexity: Complexity.Challenging,
    imageUrl:
        'https://cdn06.pramborsfm.com/storage/app/media/Prambors/Editorial/AESPA-20210211104058.jpg?tr=w-800',
    duration: 35,
    ingredients: [
      'Plaza Indonesia',
      'Gandaria City',
      'Pondok Indah Mall',
      'Plaza Senayan',
      'Lippo Mall Kemang',
      'Senayan City'
      'Lippo Mall Puri',
    ],
    steps: [
      'Essentials Fleece 3-Stripes Pants',
      'LOUNGEWEAR Essentials Logo Fleece Hoodie',
      'Ultraboost 21 Shoes',
    ],
    isGlutenFree: true,
    isVegan: false,
    isVegetarian: false,
    isLactoseFree: true,
  ),
  Meal(
    id: 'm9',
    categories: [
      'c9',
    ],
    title: 'JACQUEMUS',
    affordability: Affordability.Affordable,
    complexity: Complexity.Hard,
    imageUrl:
        'https://cdn06.pramborsfm.com/storage/app/media/Prambors/Editorial/AESPA-20210211104058.jpg?tr=w-800',
    duration: 45,
    ingredients: [
      'Plaza Indonesia',
      'Gandaria City',
      'Pondok Indah Mall',
      'Plaza Senayan',
      'Lippo Mall Kemang',
      'Senayan City'
      'Lippo Mall Puri',
      'Lotte Mall',
      'Kuningan City'
    ],
    steps: [
      'Essentials Fleece 3-Stripes Pants',
      'LOUNGEWEAR Essentials Logo Fleece Hoodie',
      'Ultraboost 21 Shoes',
    ],
    isGlutenFree: true,
    isVegan: false,
    isVegetarian: true,
    isLactoseFree: false,
  ),
  Meal(
    id: 'm10',
    categories: [
      'c2',
    ],
    title: 'KENZO',
    affordability: Affordability.Luxurious,
    complexity: Complexity.Simple,
    imageUrl:
        'https://cdn06.pramborsfm.com/storage/app/media/Prambors/Editorial/AESPA-20210211104058.jpg?tr=w-800',
    duration: 30,
    ingredients: [
      'Plaza Indonesia',
      'Gandaria City',
      'Pondok Indah Mall',
      'Plaza Senayan',
      'Lippo Mall Kemang',
    ],
    steps: [
      'Essentials Fleece 3-Stripes Pants',
      'LOUNGEWEAR Essentials Logo Fleece Hoodie',
      'Ultraboost 21 Shoes',
    ],
    isGlutenFree: true,
    isVegan: true,
    isVegetarian: true,
    isLactoseFree: true,
  ),
];
