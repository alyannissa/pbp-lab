1. Apakah perbedaan antara JSON dan XML?
   
   JSON dan XML adalah dua format paling umum untuk menyimpan dan menukar data yang diterima dari web server.  
   JSON adalah singkatan dari JavaScript Object Notation yang merupakan sebuah format file standar untuk menyimpan data 
   secara terorganisir dan dapat diakses dengan mudah yang didasarkan pada bahasa pemrograman JavaScript. Untuk setiap 
   elemen pada suatu model JSON dibuat key (kunci) dan value (nilai). Sedangkan, XML adalah singkatan dari Extensible 
   Markup Language yang merupakan sebuah markup yang berfungsi untuk melakukan penyimpanan data secara struktural dan 
   terorganisir, bukan untuk menampilkan data yang ada. XML merupakan turunan dari SGML (Standard Generalized Markup 
   Language). Markup code yang diberikan oleh XML merupakan kode yang mudah sekali dipahami oleh pihak manusia maupun 
   dari pihak komputer. XML memiliki implementasi tag yang mirip dengan HTML tetapi dimulai dari tag suatu model. 
   Didalamnya akan diisi tag dari seluruh elemen yang ada pada model tersebut.

   Dalam segi penyimpanan data XML disimpan sebagai tree structure, sedangkan JSON disimpan dalam bentuk seperti map 
   dengan pasangan key (kunci) dan value (nilai). Lalu dalam segi pengolahan data, XML dapat melakukan pemrosesan dan 
   pemformatan data pada dokumen dan objek, sedangkan pada JSON hal ini tidak dapat dilakukan. XML mendukung banyak tipe 
   data kompleks termasuk bagan, charts, dan tipe data non-primitif lainnya, sedangkan JSON hanya mendukung objek dalam 
   tipe data primitif seperti, string, integer, boolean, dan lain-lain. XML mendukung pengkodean UTF-8 dan UTF-16, 
   sedangkan JSON mendukung penyandiaksaraan UTF serta ASCII.
   
2. Apakah perbedaan antara HTML dan XML?
   
   XML dan HTML adalah bahasa markup yang ditentukan untuk tujuan berbeda dan memiliki beberapa perbedaan. HTML atau 
   biasa disebut Hypertext Markup Language memiliki fokus kepada penyajian data dan merupakan bahasa yang tidak peka 
   dengan Case Sensitive. Sedangkan, XML memiliki fokus pada transfer data, bukan untuk menampilkan data dan merupakan 
   bahasa yang memperhatikan Case Sensitive. XML tidak mengizinkan kesalahan apa pun jika ada beberapa kesalahan dalam 
   kode yang tidak dapat diuraikan. Sebaliknya, dalam HTML kesalahan kecil dapat diabaikan. Pada penulisan kode, tag 
   dalam XML wajib ditutup, sedangkan dalam HTML tag terbuka dapat berfungsi dengan baik. Jadi, XML dan HTML saling 
   melengkapi dan terkait satu sama lain karena mempunyai fungsi yang berbeda yaitu di mana HTML digunakan untuk 
   presentasi data sedangkan tujuan utama XML adalah untuk menyimpan dan mentransfer data. 






Referensi :
https://www.monitorteknologi.com/perbedaan-json-dan-xml/
https://id.sawakinome.com/articles/protocols--formats/difference-between-json-and-xml-3.html
https://www.geeksforgeeks.org/difference-between-json-and-xml/
https://id.gadget-info.com/difference-between-xml
https://blogs.masterweb.com/perbedaan-xml-dan-html/