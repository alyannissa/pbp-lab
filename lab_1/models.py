from django.db import models

# TODO Create Friend model that contains name, npm, and DOB (date of birth) here

class Friend(models.Model):
    name = models.CharField(max_length=30) #CharField -> input yang dimasukin itu tipenya text
    # TODO Implement missing attributes in Friend model
    npm = models.IntegerField() #IntegerField -> input yang dimasukin itu tipenya angka
    dob = models.DateField()

